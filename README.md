# Ma Grille d'Évaluation (MGE)

**Ma Grille d'Évaluation (MGE)** est une application web permettant de générer et gérer des grilles d'évaluation pour les enseignants. Cette application permet de créer des grilles d'évaluations commentées oralement, de fournir des retours aux étudiants, et d'analyser les performances des élèves.

## Fonctionnalités

- 📑 **Gestion des évaluations** : Créez et organisez vos grilles d'évaluation avec flexibilité.
- 📊 **Visualisation intuitive** : Parcourez les grilles d'évaluation grâce à un affichage en carrousel.
- ✍ **Correction enrichie 🗣️** : Fournissez des commentaires textuels et audio pour une correction détaillée.
- 📈 **Analyse statistique** : Accédez à des analyses pour mieux comprendre les performances des élèves.
- 📝 **Édition et sauvegarde efficaces** : Modifiez et enregistrez les grilles d'évaluations en temps réel.
- 🎯 **Accessibilité et sécurité** : Profitez d'une plateforme sécurisée et respectueuse de la vie privée.

### Documentation
https://stmg-bts.notion.site/Pr-sentation-de-MGE-Ma-grille-d-valuation-8fb439500f6749d4b3ce55f265aa36dd?pvs=4l

### Prérequis

- Serveur web (Apache, Nginx, etc.)
- PHP >= 8.0
- MySQL >= 5.7

### Étapes d'installation

1. **Téléchargez le projet**
2. **Déposez les fichiers sur votre serveur web**
3. **Ouvrez installation.php et laissez-vous guider**

## Licence

Ce projet est sous licence Creative Commons Attribution-NonCommercial 4.0 International (CC BY-NC 4.0). Cela signifie que vous êtes libre de partager et d'adapter le matériel pour des utilisations non commerciales, tant que vous créditez l'œuvre et que vous n'appliquez pas de restrictions supplémentaires. Voir le fichier [LICENSE](LICENSE) pour plus de détails.
