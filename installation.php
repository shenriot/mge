<?php
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    // Récupération des informations du formulaire
    $db_host = $_POST['db_host'];
    $db_name = $_POST['db_name'];
    $db_user = $_POST['db_user'];
    $db_password = $_POST['db_password'] ?? ''; // Rendre le mot de passe optionnel
    $admin_username = $_POST['admin_username'];
    $admin_password = password_hash($_POST['admin_password'], PASSWORD_BCRYPT); // Hachage du mot de passe
    $admin_email = $_POST['admin_email'];

    // Connexion au serveur MySQL
    $mysqli = new mysqli($db_host, $db_user, $db_password);

    if ($mysqli->connect_error) {
        die('Erreur de connexion (' . $mysqli->connect_errno . ') ' . $mysqli->connect_error);
    }

    // Création de la base de données
    $create_db_query = "CREATE DATABASE IF NOT EXISTS $db_name";
    if ($mysqli->query($create_db_query) !== TRUE) {
        die('Erreur lors de la création de la base de données: ' . $mysqli->error);
    }

    // Sélection de la base de données
    $mysqli->select_db($db_name);

    // Création des tables
    $queries = [
        "DROP TABLE IF EXISTS `evaluations`;",
        "CREATE TABLE IF NOT EXISTS `evaluations` (
            `evaluation_id` int NOT NULL AUTO_INCREMENT,
            `name` varchar(255) COLLATE utf8mb4_general_ci NOT NULL,
            `evaluation_date` date NOT NULL,
            `coefficient` decimal(3,2) NOT NULL,
            `teacher_note` text COLLATE utf8mb4_general_ci,
            `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
            `archive` tinyint(1) NOT NULL DEFAULT '0',
            `created_by` int DEFAULT NULL,
            `consignes_correction` text COLLATE utf8mb4_general_ci,
            `is_favorite` tinyint(1) DEFAULT '0',
            `mge_code` varchar(6) COLLATE utf8mb4_general_ci DEFAULT NULL,
            `entered_in_ENT` tinyint(1) DEFAULT '0',
            PRIMARY KEY (`evaluation_id`),
            UNIQUE KEY `mge_code` (`mge_code`),
            KEY `created_by` (`created_by`)
        ) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;",
        
        "DROP TABLE IF EXISTS `parts`;",
        "CREATE TABLE IF NOT EXISTS `parts` (
            `part_id` int NOT NULL AUTO_INCREMENT,
            `evaluation_id` int NOT NULL,
            `name` varchar(255) COLLATE utf8mb4_general_ci NOT NULL,
            `weight` int DEFAULT '1',
            `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
            `order_index` int DEFAULT NULL,
            PRIMARY KEY (`part_id`),
            KEY `evaluation_id` (`evaluation_id`)
        ) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;",
        
        "DROP TABLE IF EXISTS `student_copies`;",
        "CREATE TABLE IF NOT EXISTS `student_copies` (
            `copy_id` int NOT NULL AUTO_INCREMENT,
            `evaluation_id` int NOT NULL,
            `student_name` varchar(255) COLLATE utf8mb4_general_ci NOT NULL,
            `general_comment` text COLLATE utf8mb4_general_ci,
            `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
            `updated_at` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
            `bonus_points` decimal(4,2) DEFAULT '0.00',
            PRIMARY KEY (`copy_id`)
        ) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;",
        
        "DROP TABLE IF EXISTS `subparts`;",
        "CREATE TABLE IF NOT EXISTS `subparts` (
            `subpart_id` int NOT NULL AUTO_INCREMENT,
            `part_id` int NOT NULL,
            `name` varchar(255) COLLATE utf8mb4_general_ci NOT NULL,
            `criteria` text COLLATE utf8mb4_general_ci NOT NULL,
            `points` decimal(5,2) NOT NULL,
            `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
            `order_index` int DEFAULT NULL,
            PRIMARY KEY (`subpart_id`),
            KEY `part_id` (`part_id`)
        ) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;",
        
        "DROP TABLE IF EXISTS `subpart_evaluations`;",
        "CREATE TABLE IF NOT EXISTS `subpart_evaluations` (
            `subpart_evaluation_id` int NOT NULL AUTO_INCREMENT,
            `copy_id` int NOT NULL,
            `subpart_id` int NOT NULL,
            `evaluation_criteria` decimal(10,2) DEFAULT NULL,
            `comment` text COLLATE utf8mb4_general_ci,
            `audio_comment` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL,
            PRIMARY KEY (`subpart_evaluation_id`),
            UNIQUE KEY `unique_subpart_copy` (`copy_id`,`subpart_id`)
        ) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;",
        
        "DROP TABLE IF EXISTS `users`;",
        "CREATE TABLE IF NOT EXISTS `users` (
            `id` int NOT NULL AUTO_INCREMENT,
            `username` varchar(50) COLLATE utf8mb4_general_ci NOT NULL,
            `password` varchar(255) COLLATE utf8mb4_general_ci NOT NULL,
            `email` varchar(100) COLLATE utf8mb4_general_ci DEFAULT NULL,
            `role` enum('superadmin','professeur') COLLATE utf8mb4_general_ci NOT NULL DEFAULT 'professeur',
            `approved` tinyint(1) NOT NULL DEFAULT '0',
            PRIMARY KEY (`id`),
            UNIQUE KEY `username` (`username`)
        ) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;"
    ];

    foreach ($queries as $query) {
        if ($mysqli->query($query) !== TRUE) {
            echo "Erreur lors de l'exécution de la requête: " . $mysqli->error;
        }
    }

    // Création de l'utilisateur administrateur
    $stmt = $mysqli->prepare("INSERT INTO `users` (username, password, email, role, approved) VALUES (?, ?, ?, 'superadmin', 1)");
    $stmt->bind_param('sss', $admin_username, $admin_password, $admin_email);

    $messages = [];

    if ($stmt->execute() === TRUE) {
        $messages[] = "Utilisateur administrateur créé avec succès.";
    } else {
        $messages[] = "Erreur lors de la création de l'utilisateur administrateur: " . $stmt->error;
    }

    // Décompression de l'archive
    $zip = new ZipArchive;
    $res = $zip->open(__DIR__ . '/MGE.zip'); // Chemin vers l'archive
    if ($res === TRUE) {
        $zip->extractTo(__DIR__); // Extraction à la racine du dossier actuel
        $zip->close();
        $messages[] = "Archive décompressée avec succès.";
    } else {
        $messages[] = "Erreur lors de la décompression de l'archive.";
    }

    // Modification des paramètres de connexion à la base de données dans db_connect.php
    $db_connect_path = __DIR__ . '/db_connect.php';
    if (file_exists($db_connect_path)) {
        $db_connect_content = file_get_contents($db_connect_path);
        $db_connect_content = preg_replace([
            '/\$host\s*=\s*".*";/',
            '/\$username\s*=\s*".*";/',
            '/\$password\s*=\s*".*";/',
            '/\$database\s*=\s*".*";/'
        ], [
            "\$host = \"$db_host\";",
            "\$username = \"$db_user\";",
            "\$password = \"$db_password\";",
            "\$database = \"$db_name\";"
        ], $db_connect_content);
        file_put_contents($db_connect_path, $db_connect_content);
        $messages[] = "Fichier db_connect.php mis à jour avec succès.";
    } else {
        $messages[] = "Erreur : fichier db_connect.php introuvable.";
    }

    $mysqli->close();
    ?>

    <!DOCTYPE html>
    <html lang="fr">
    <head>
        <title>Configuration terminée</title>
        <!-- Inclusion de Bootstrap CSS -->
        <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" rel="stylesheet">
        <link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
        <style>
            body {
                font-family: Arial, sans-serif;
                background-color: #f2f2f2;
                display: flex;
                justify-content: center;
                align-items: center;
                height: 100vh;
            }
            .container {
                background-color: #fff;
                padding: 20px;
                border-radius: 8px;
                box-shadow: 0 0 10px rgba(0, 0, 0, 0.1);
                max-width: 600px;
                width: 100%;
            }
            .btn-container {
                text-align: center;
                margin-top: 20px;
            }
        </style>
    </head>
    <body>
        <div class="container">
            <h2 class="text-center">Configuration terminée</h2>
            <div class="alert alert-success">
                <?php foreach ($messages as $message) {
                    echo "<p>$message</p>";
                } ?>
                <p>Pour des raisons de sécurité, veuillez supprimer les fichiers <strong>installation.php</strong> et <strong>MGE.zip</strong> du serveur.</p>
            </div>
            <div class="btn-container">
                <a href="index.php" class="btn btn-primary">Aller à la page de connexion</a>
            </div>
        </div>
        <!-- Inclusion de Bootstrap JS, Popper.js, et jQuery -->
        <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.0.6/dist/umd/popper.min.js"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
    </body>
    </html>
    <?php
} else {
    // Affichage du formulaire HTML avec une mise en forme améliorée
    echo '
<!DOCTYPE html>
<html lang="fr">
<head>
    <title>Configuration de l\'application</title>
    <!-- Inclusion de Bootstrap CSS -->
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" rel="stylesheet">
    <link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
    <style>
        .logo {
            text-align: center;
            padding: 20px;
            border-radius: 10px;
            background-color: white;
            box-shadow: 0 4px 8px rgba(0, 0, 0, 0.1);
        }
        .logo-acronyme {
            display: block;
            font-size: 60px;
            font-weight: bold;
            color: #007bff;
            text-shadow: 2px 2px 4px rgba(0, 0, 0, 0.2);
        }
        .logo-intitule {
            font-size: 20px;
            color: #333;
            margin-top: 10px;
            letter-spacing: 1px;
        }
        .form-container {
            padding: 20px;
            background-color: #fff;
            border-radius: 15px;
            box-shadow: 0 4px 8px rgba(0, 0, 0, 0.1);
            max-width: 500px; /* Limite la largeur du formulaire */
            margin: 0 auto; /* Centre le formulaire horizontalement */
        }
        fieldset {
            margin-bottom: 20px;
            padding: 10px;
            border: 1px solid #ddd;
            border-radius: 5px;
        }
        legend {
            font-size: 18px;
            font-weight: bold;
            padding: 0 10px;
        }
    </style>
</head>
<body>
    <div class="logo">
        <span class="logo-acronyme">MGE</span>
        <span class="logo-intitule">Ma grille d\'évaluation</span>
    </div>
    <div class="container mt-5">
        <div class="form-container">
            <h2 class="text-center">Configuration de l\'application</h2>
            <form method="post" action="">
                <fieldset>
                    <legend>Configuration de la base de données</legend>
                    <div class="form-group">
                        <label for="db_host">Hôte de la base de données:</label>
                        <input type="text" class="form-control" id="db_host" name="db_host" required>
                    </div>
                    <div class="form-group">
                        <label for="db_name">Nom de la base de données:</label>
                        <input type="text" class="form-control" id="db_name" name="db_name" required>
                    </div>
                    <div class="form-group">
                        <label for="db_user">Utilisateur de la base de données:</label>
                        <input type="text" class="form-control" id="db_user" name="db_user" required>
                    </div>
                    <div class="form-group">
                        <label for="db_password">Mot de passe de la base de données:</label>
                        <input type="password" class="form-control" id="db_password" name="db_password">
                    </div>
                </fieldset>
                <fieldset>
                    <legend>Création de l\'administrateur</legend>
                    <div class="form-group">
                        <label for="admin_username">Nom d\'utilisateur administrateur:</label>
                        <input type="text" class="form-control" id="admin_username" name="admin_username" required>
                    </div>
                    <div class="form-group">
                        <label for="admin_password">Mot de passe administrateur:</label>
                        <input type="password" class="form-control" id="admin_password" name="admin_password" required>
                    </div>
                    <div class="form-group">
                        <label for="admin_email">Email administrateur:</label>
                        <input type="email" class="form-control" id="admin_email" name="admin_email" required>
                    </div>
                </fieldset>
                <button type="submit" class="btn btn-primary btn-block">Installer</button>
            </form>
        </div>
    </div>
    <!-- Inclusion de Bootstrap JS, Popper.js, et jQuery -->
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.0.6/dist/umd/popper.min.js"></script>
    <script src="https://stackpath.amazonaws.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
</body>
</html>';
}
?>
